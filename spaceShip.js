// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`



// 2. Call the constructor with a couple ships, 
// and call accelerate on both of them.


// unnamed
let SpaceShip = class {
  constructor(name, topSpeed) {
    this.name = name;
    this.topSpeed = topSpeed;
  }
  accelerate() {
    return console.log(`${this.name} moving to ${this.topSpeed}`);
  }
};

  //2.

let spaceShip1 = new SpaceShip ("Rocket", 50)

let spaceShip2 = new SpaceShip ("Spencer", 100)

spaceShip1.accelerate();
spaceShip2.accelerate();

// output: "Rectangle"

// named
//let Rectangle = class Rectangle2 {
  //constructor(height, width) {
    //this.height = height;
    //this.width = width;
  //}
//};
//console.log(Rectangle.name);
// output: "Rectangle2"